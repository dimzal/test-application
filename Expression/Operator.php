<?php

namespace App\Expression;

class Operator extends Token {
    const O_L_ASSOCIATIVE = -1;
    const O_N_ASSOCIATIVE = 0;
    const O_R_ASSOCIATIVE = 1;

    protected $priority;
    protected $associativity;

    public static $operatorsPriority = [
        '-' => [ 'priority' => 0, 'associativity' => self::O_L_ASSOCIATIVE ],
        '+' => [ 'priority' => 0, 'associativity' => self::O_L_ASSOCIATIVE ],
        '*' => [ 'priority' => 1, 'associativity' => self::O_L_ASSOCIATIVE ],
        '/' => [ 'priority' => 1, 'associativity' => self::O_L_ASSOCIATIVE ],
    ];

    public function __construct($value, $priority, $associativity)
    {
        $this->priority = (int) $priority;
        $this->associativity = (int) $associativity;

        parent::__construct($value, Token::T_OPERATOR);
    }

    public function getAssociativity()
    {
        return $this->associativity;
    }

    public function getPriority()
    {
        return $this->priority;
    }

    public function hasLowerPriority(Operator $o)
    {
        $hasLowerPriority = ((Operator::O_L_ASSOCIATIVE == $o->getAssociativity() && $this->getPriority() == $o->getPriority()) || $this->getPriority() < $o->getPriority());
        return $hasLowerPriority;
    }
}