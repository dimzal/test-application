<?php

namespace App\Parser;

class InputStringFactory implements ParserFactory
{
    public function createParserFactory(): Parser
    {
        return new InputStringParser();
    }
}