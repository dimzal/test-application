FROM ubuntu:16.04
MAINTAINER Dmitry Zalevskiy

RUN \
    apt-get update && \
    apt-get install -y software-properties-common vim curl wget git

# Install PHP 7.2 with modules and composer
RUN \
  apt-get install -y \
  php7.2 \
  php7.2-cli \
  php7.2-common \
  php7.2-curl \
  php7.2-fpm \
  php7.2-json \
  php7.2-mbstring \
  php7.2-intl

RUN sed -i -e "s/;\?daemonize\s*=\s*yes/daemonize = no/g" /etc/php/7.2/fpm/php-fpm.conf

RUN curl -sS https://getcomposer.org/installer | php -- \
        --filename=composer \
        --install-dir=/usr/local/bin