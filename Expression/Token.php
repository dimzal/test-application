<?php

namespace App\Expression;

class Token
{
    const T_OPERATOR = 1;
    const T_OPERAND = 2;
    const T_LEFT_BRACKET = 3;
    const T_RIGHT_BRACKET = 4;

    protected $value;

    protected $type;

    public function __construct($value, $type)
    {
        $this->value = $value;
        $this->type = $type;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getType()
    {
        return $this->type;
    }
}