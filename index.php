<?php


$inputStringFactory = new \App\Parser\InputStringFactory();
$parser = $inputStringFactory->createParserFactory();

$parser->setExpression("1+2*3*(7*8)-(45-10)");
$parser->calculateResult();

echo $parser->getResults();