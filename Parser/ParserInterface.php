<?php

namespace App\Parser;

interface Parser
{
    public function setExpression(string $expression);
    public function calculateResult(): void;
    public function getResults(): int;
}