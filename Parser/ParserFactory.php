<?php

namespace App\Parser;

interface ParserFactory
{
    public function createParserFactory(): Parser;
}