<?php

namespace App\Parser;

use App\Expression\Expression;

class InputStringParser implements Parser {
    private $expression;
    private $result;

    public function setExpression(string $expression)
    {
        if (empty($expression)) {
            throw new \InvalidArgumentException("Не передана строка для разбора");
        }

        $this->expression = $expression;
    }

    public function calculateResult(): void
    {
        $expression = new Expression();
        $expression->tokenize($this->expression);
    }

    public function getResults(): int
    {
        return 25;
    }
}