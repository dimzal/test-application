<?php
namespace App\Expression;

use InvalidArgumentException;

use SplQueue;
use SplStack;

class ShuntingYard
{
    private $operatorStack;

    private $outputQueue;

    public function translate(array $tokens)
    {
        $this->operatorStack = new SplStack();
        $this->outputQueue = new SplQueue();

        foreach($tokens as $token) {
            switch ($token->getType()) {
                case Token::T_OPERAND:
                    $this->outputQueue->enqueue($token);
                    break;
                case Token::T_OPERATOR:
                    $o1 = $token;
                    while($this->hasOperatorInStack() && ($o2 = $this->operatorStack->top()) && $o1->hasLowerPriority($o2)) {
                        $this->outputQueue->enqueue($this->operatorStack->pop());
                    }
                    $this->operatorStack->push($o1);
                    break;
                case Token::T_LEFT_BRACKET:
                    $this->operatorStack->push($token);
                    break;
                case Token::T_RIGHT_BRACKET:
                    while((!$this->operatorStack->isEmpty()) && (Token::T_LEFT_BRACKET != $this->operatorStack->top()->getType())) {
                        $this->outputQueue->enqueue($this->operatorStack->pop());
                    }
                    $this->operatorStack->pop();
                    break;
                default:
                    throw new InvalidArgumentException(sprintf('Invalid token detected: %s', $token));
                    break;
            }
        }
        while($this->hasOperatorInStack()) {
            $this->outputQueue->enqueue($this->operatorStack->pop());
        }
        if(!$this->operatorStack->isEmpty()) {
            throw new InvalidArgumentException(sprintf('Mismatched parenthesis or misplaced number: %s', implode(' ',$tokens)));
        }
        return iterator_to_array($this->outputQueue);
    }

    private function hasOperatorInStack()
    {
        $hasOperatorInStack = false;
        if(!$this->operatorStack->isEmpty()) {
            $top = $this->operatorStack->top();

            if(Token::T_OPERATOR == $top->getType()) {
                $hasOperatorInStack = true;
            }
        }
        return $hasOperatorInStack;
    }
}