<?php
namespace App\Expression;

use App\Expression\Token;

class Expression {
    private $tokens;
    private $expression;

    private static $allowedSymbols = [
        '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '(', ')', '/', '*', '+', '-'
    ];

    private static $operatorsSymbols = [
        '(', ')', '/', '*', '+', '-'
    ];

    public function tokenize(string $code) {
        $this->expression = $code;
        $this->tokens = [];

        $tokenArray = $this->parseString($code);

        foreach ($tokenArray as $t) {
            echo $t."\r\n";

            if (array_key_exists($t, Operator::$operatorsPriority)) {
                $token = new Operator($t, Operator::$operatorsPriority[$t]['priority'], Operator::$operatorsPriority[$t]['associativity']);
            } elseif (is_numeric($t)) {
                $token = new Token((float) $t, Token::T_OPERAND);
            }elseif('(' === $t) {
                $token = new Token($t, Token::T_LEFT_BRACKET);
            }elseif(')' === $t) {
                $token = new Token($t, Token::T_RIGHT_BRACKET);
            }

            $this->tokens[] = $token;
        }

        $shuntingYard = new ShuntingYard();
        $expressionTokens = $shuntingYard->translate($this->tokens);


        $stack = new \SplStack();
        foreach ($expressionTokens as $token) {
            $tokenValue = $token->getValue();

            if (is_numeric($tokenValue)) {
                $stack->push((float) $tokenValue);
                continue;
            }

            switch ($tokenValue) {
                case '+':
                    $stack->push($stack->pop() + $stack->pop());
                    break;
                case '-':
                    $n = $stack->pop();
                    $stack->push($stack->pop() - $n);
                    break;
                case '*':
                    $stack->push($stack->pop() * $stack->pop());
                    break;
                case '/':
                    $n = $stack->pop();
                    $stack->push($stack->pop() / $n);
                    break;
                case '%':
                    $n = $stack->pop();
                    $stack->push($stack->pop() % $n);
                    break;
                default:
                    throw new \InvalidArgumentException(sprintf('Invalid operator detected: %s', $tokenValue));
                    break;
            }
        }

        return $stack->top();
    }

    private function parseString(string $expression): array {
        $decimalNumbers = array_diff(self::$allowedSymbols, self::$operatorsSymbols);

        $tokens = []; $i = 0; $buff = '';

        while ($i < strlen($expression)) {
            if (!in_array($expression[$i], self::$allowedSymbols)) {
                throw new \InvalidArgumentException("В строке есть недопустимые символы");
            }

            if (in_array($expression[$i], $decimalNumbers)) {
                $buff .= $expression[$i];
            } else {
                $tokens[] = $buff;
                $tokens[] = $expression[$i];

                $buff = '';
            }

            $i++;
        }

        return $tokens;
    }
}